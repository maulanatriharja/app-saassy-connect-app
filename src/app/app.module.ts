import { HttpClientModule } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { HTTP } from '@ionic-native/http/ngx';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { MediaCapture } from '@ionic-native/media-capture/ngx';
import { Media } from '@ionic-native/media/ngx';
import { WebIntent } from '@ionic-native/web-intent/ngx';

import { RecordAudioPage } from './pages/record-audio/record-audio.page';
import { ContactsModalPage } from './pages/contacts-modal/contacts-modal.page';
import { SharePage } from './pages/share/share.page';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    })
  ],
  declarations: [
    AppComponent,
    RecordAudioPage,
    ContactsModalPage,
    SharePage,
  ],
  entryComponents: [
    RecordAudioPage,
    ContactsModalPage,
    SharePage,
  ],
  exports: [
    // RecordAudioPage,
  ],
  providers: [
    Camera,
    Dialogs,
    FilePath,
    HTTP,
    InAppBrowser,
    SocialSharing,
    SpinnerDialog,
    SplashScreen,
    StatusBar,
    FirebaseX,
    FileOpener,
    FileTransfer,
    File,
    FileChooser,
    MediaCapture,
    Media,
    WebIntent
  ],
  bootstrap: [
    AppComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }

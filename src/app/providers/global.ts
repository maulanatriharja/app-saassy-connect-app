import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class GlobalProvider {

  private data_login = new Subject<any>();
  pubLogin(data: any) { this.data_login.next(data); }
  obvLogin(): Subject<any> { return this.data_login; }

  private data_profile = new Subject<any>();
  pubProfile(data: any) { this.data_profile.next(data); }
  obvProfile(): Subject<any> { return this.data_profile; }

  private data_audio = new Subject<any>();
  pubAudio(data: any) { this.data_audio.next(data); }
  obvAudio(): Subject<any> { return this.data_audio; }

  //----------
  public level: string;
  public whitelable: string;
  public app_name: string;
  public baseURL: string;

  constructor() {
    this.level = 'production';
    // this.level = 'beta'; 

    // this.whitelable = 'saassy';
    // this.whitelable = 'quizminer';
    this.whitelable = 'pandaimu';

    if (this.whitelable == 'saassy') {
      this.app_name = 'SAASSY CONNECT';

      if (this.level == 'production') {
        this.baseURL = "https://growth.saassy.co/api/mobile/v1"; //PRODUCTION
      } else {
        this.baseURL = "https://beta.growth.marketingmesh.co/api/mobile/v1"; //BETA
      }
    }

    if (this.whitelable == 'quizminer') {
      this.app_name = 'QUIZMINER';

      if (this.level == 'production') {
        this.baseURL = "https://growth.quizminer.com/api/mobile/v1"; //PRODUCTION
      } else {
        this.baseURL = "https://beta.growth.marketingmesh.co/api/mobile/v1"; //BETA
      }
    }

    if (this.whitelable == 'pandaimu') {
      this.app_name = 'PANDAIMU';

      if (this.level == 'production') {
        this.baseURL = "https://growth.pandaimu.com/api/mobile/v1"; //PRODUCTION
      } else {
        this.baseURL = "https://beta.growth.marketingmesh.co/api/mobile/v1"; //BETA
      }
    }
  }


}

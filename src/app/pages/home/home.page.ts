import { Component } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { Platform, ToastController } from '@ionic/angular';
import { GlobalProvider } from '../../providers/global';
import { Router } from '@angular/router';


@Component({
  selector: 'home',
  templateUrl: './home.page.html',
  styleUrls: [ './home.page.scss' ],
})

export class HomePage {

  loading: boolean = true;

  data: any = [];
  refresh_event: any;

  sort: string;

  backed: number = 0;
  constructor(
    public global: GlobalProvider,
    public http: HTTP,
    public platform: Platform,
    public router: Router,
    public storage: Storage,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
    // this.platform.backButton.subscribe(async () => {
    //   navigator[ 'app' ].exitApp();
    // });
  }

  ionViewWillEnter() {
    // this.load_profile();
  }

  ionViewDidEnter() {
    this.load_campaigns();
    this.backToExit();
  }

  // load_profile() {
  //   this.storage.get('token').then((val_token) => {
  //     let url = this.global.baseURL + '/profile';
  //     let body = {};
  //     let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

  //     this.http.get(url, body, options).then(async res => {
  //       console.log(res.data);

  //       res.data = JSON.parse(res.data);

  //       this.data = res.data;
  //     }).catch(async error => {
  //       console.log(error.error);

  //       error.error = JSON.parse(error.error);
  //     });
  //   });
  // }

  load_campaigns() {
    this.data = [];

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/workspaces';
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(res_all => {
        console.log(res_all.data);

        res_all.data = JSON.parse(res_all.data);

        for (let a = 0; a < res_all.data.length; a++) {
          let url = this.global.baseURL + '/workspace/' + res_all.data[ a ].id + '/teams';
          let body = {};
          let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

          this.http.get(url, body, options).then(res_w => {
            console.log(res_w.data);

            res_w.data = JSON.parse(res_w.data);

            for (let x = 0; x < res_w.data.length; x++) {
              this.storage.get('token').then((val_token) => {
                let url = this.global.baseURL + '/team/' + res_w.data[ x ].id + '/projects';
                let body = {};
                let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

                this.http.get(url, body, options).then(res_p => {
                  console.log(res_p.data);

                  res_p.data = JSON.parse(res_p.data);

                  for (let n = 0; n < res_p.data.length; n++) {
                    let url = this.global.baseURL + '/project/' + res_p.data[ n ].id + '/campaigns';
                    let body = {};
                    let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

                    this.http.get(url, body, options).then(res => {
                      console.log(res.data);

                      res.data = JSON.parse(res.data);

                      for (let i = 0; i < res.data.length; i++) {
                        if (res.data[ i ].analytic == '') {
                          res.data[ i ].analytic.view = 0;
                          res.data[ i ].analytic.lead = 0;
                          res.data[ i ].analytic.conversion = 0;
                        }
                        this.data.push(res.data[ i ]);
                      }

                      this.loading = false;
                      this.refresh_event.target.complete();

                    }).catch(async error => {
                      console.log(error.error);

                      error.error = JSON.parse(error.error);
                    });
                  }
                }).catch(async error => {
                  console.log(error.error);

                  error.error = JSON.parse(error.error);
                });
              });
            }
          }).catch(async error => {
            console.log(error.error);

            error.error = JSON.parse(error.error);
          });
        }
      }).catch(async error => {
        console.log(error.error);

        error.error = JSON.parse(error.error);
      });
    });
  }

  sort_campaign() {
    if (this.sort != 'campaign_asc') {
      this.data.sort((a, b) => a.name.localeCompare(b.name));
      this.sort = 'campaign_asc';
    } else {
      this.data.sort((a, b) => b.name.localeCompare(a.name));
      this.sort = 'campaign_desc';
    }
  }

  sort_view() {
    if (this.sort != 'view_asc') {
      this.data.sort(function (a, b) { return parseInt(a.analytic.view) - parseInt(b.analytic.view) });
      this.sort = 'view_asc';
    } else {
      this.data.sort(function (a, b) { return parseInt(b.analytic.view) - parseInt(a.analytic.view) });
      this.sort = 'view_desc';
    }
  }

  sort_lead() {
    if (this.sort != 'lead_asc') {
      this.data.sort(function (a, b) { return parseInt(a.analytic.lead) - parseInt(b.analytic.lead) });
      this.sort = 'lead_asc';
    } else {
      this.data.sort(function (a, b) { return parseInt(b.analytic.lead) - parseInt(a.analytic.lead) });
      this.sort = 'lead_desc';
    }
  }

  sort_conversion() {
    if (this.sort != 'conversion_asc') {
      this.data.sort(function (a, b) { return parseInt(a.analytic.conversion) - parseInt(b.analytic.conversion) });
      this.sort = 'conversion_asc';
    } else {
      this.data.sort(function (a, b) { return parseInt(b.analytic.conversion) - parseInt(a.analytic.conversion) });
      this.sort = 'conversion_desc';
    }
  }

  backToExit() {
    this.backed = 0;

    this.platform.backButton.subscribe(async () => {
      if (this.router.url == '/home' && this.backed == 0) {
        const toast = await this.toastController.create({
          header: 'Press back again to exit.',
          position: 'bottom',
          color: 'dark',
          duration: 2000
        });
        toast.present();

        this.backed = 1;
        this.platform.backButton.subscribe(async () => {
          if (this.router.url == '/home' && this.backed == 1) {
            navigator[ 'app' ].exitApp();
          }
        });
      }
    });
  }

}

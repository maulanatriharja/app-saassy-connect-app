import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActionSheetController, ModalController, Platform, ToastController } from '@ionic/angular';
import { InAppBrowser, InAppBrowserEvent } from '@ionic-native/in-app-browser/ngx';
import { GlobalProvider } from '../../providers/global';
import { Router } from '@angular/router';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';

import { ContactsModalPage } from '../contacts-modal/contacts-modal.page';

@Component({
    selector: 'campaigns',
    templateUrl: './campaigns.page.html',
    styleUrls: [ './campaigns.page.scss' ],
})
export class CampaignsPage implements OnInit {

    loading: boolean = true;

    data: any = [];

    project_id: string;

    refresh_event: any;

    backed: number = 0;
    constructor(
        public actionSheetController: ActionSheetController,
        public dialogs: Dialogs,
        public global: GlobalProvider,
        public http: HTTP,
        public iab: InAppBrowser,
        public modalController: ModalController,
        public platform: Platform,
        public route: ActivatedRoute,
        public router: Router,
        public socialSharing: SocialSharing,
        public spinnerDialog: SpinnerDialog,
        public storage: Storage,
        public toastController: ToastController,
    ) {
        // this.events.subscribe('p_id_changed', (val) => {
        //     this.loading = true;
        //     this.load_campaigns();
        //     this.project_id = val;
        // });
    }

    async share(val_url, val_campaign_id) {
        const actionSheet = await this.actionSheetController.create({
            header: 'Share',
            buttons: [ {
                text: 'Share to anyone',
                icon: 'people-circle-outline',
                handler: () => {
                    let options = {
                        url: val_url
                    };
                    this.socialSharing.shareWithOptions(options);
                }
            }, {
                text: 'share to CRM contact',
                icon: 'person-circle-outline',
                handler: async () => {
                    const modal = await this.modalController.create({
                        component: ContactsModalPage,
                        componentProps: {
                            url: val_url,
                            team_id: null,
                            campaign_id: val_campaign_id,
                        }
                    });
                    modal.present();
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                }
            } ]
        });
        await actionSheet.present();
    }

    ngOnInit() {
        // setInterval(() => {
        this.load_campaigns();
        // }, 3000);
    }

    ionViewDidEnter() {
        this.backed = 0;

        this.platform.backButton.subscribe(async () => {
            if (this.router.url.startsWith('/campaigns') == true && this.backed == 0) {
                const toast = await this.toastController.create({
                    header: 'Press back again to exit.',
                    position: 'bottom',
                    color: 'dark',
                    duration: 2000
                });
                toast.present();

                this.backed = 1;
                this.platform.backButton.subscribe(async () => {
                    if (this.router.url.startsWith('/campaigns') == true && this.backed == 1) {
                        navigator[ 'app' ].exitApp();
                    }
                });
            }
        });
    }

    doRefresh(event) {
        this.loading = true;

        this.refresh_event = event;
        this.load_campaigns()
    }

    load_campaigns() {
        this.data = [];

        this.project_id = this.route.snapshot.paramMap.get('project_id');

        this.storage.get('token').then((val_token) => {
            let url = this.global.baseURL + '/project/' + this.project_id + '/campaigns';
            let body = {};
            let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

            this.http.get(url, body, options).then(res => {
                console.log(res.data);

                res.data = JSON.parse(res.data);

                for (let i = 0; i < res.data.length; i++) {
                    this.data.push(res.data[ i ]);
                }

                // alert(JSON.stringify(res.data[0]));

                this.loading = false;
                this.refresh_event.target.complete();

            }).catch(async error => {
                console.log(error.error);

                error.error = JSON.parse(error.error);
            });
        });
    }

    openIAB(val_campaign_name, val_url) {
        // this.router.navigate([ '/privatebrowser/' + '/' + val_campaign_name + '/' + val_url.replace("/", "%2F").replace("/", "%2F").replace("/", "%2F").replace("/", "%2F") ]);

        let iab_load = this.iab.create(val_url, '_blank', { hideurlbar: 'yes' });

        iab_load.on('loadstart').subscribe((event: InAppBrowserEvent) => {
            console.info('LOADSTART');
            console.info("LOG: API Response");
            console.info('EVENT ::: ' + JSON.stringify(event));

            this.spinnerDialog.show();
        });

        iab_load.on('loadstop').subscribe((event: InAppBrowserEvent) => {
            console.info('LOADSTOP');
            console.info("LOG: API Response");
            console.info('EVENT ::: ' + JSON.stringify(event));

            this.spinnerDialog.hide();
        });
    }

    openBrowser(val_url) {
        this.iab.create(val_url, '_system', { hideurlbar: 'yes' });
    }

}

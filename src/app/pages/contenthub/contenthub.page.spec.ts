import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContenthubPage } from './contenthub.page';

describe('ContenthubPage', () => {
  let component: ContenthubPage;
  let fixture: ComponentFixture<ContenthubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContenthubPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContenthubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

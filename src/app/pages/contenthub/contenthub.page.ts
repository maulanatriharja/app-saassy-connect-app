import { ActionSheetController, AlertController, LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { MediaCapture, MediaFile, CaptureError } from '@ionic-native/media-capture/ngx';
import { Component, NgZone } from '@angular/core';
import { GlobalProvider } from '../../providers/global';
import { ActivatedRoute } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router';
import { Media } from '@ionic-native/media/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { File } from '@ionic-native/file/ngx';

import { RecordAudioPage } from '../record-audio/record-audio.page';
import { ContactsModalPage } from '../contacts-modal/contacts-modal.page';
import { SharePage } from '../share/share.page';

@Component({
	selector: 'contenthub',
	templateUrl: './contenthub.page.html',
	styleUrls: [ './contenthub.page.scss' ],
})
export class ContenthubPage {

	loading: boolean;

	sgm_content = 'image';

	s_workspace: string = '';
	s_workspace_default: string;
	s_team: string = '';

	workspace_id: string;
	data_workspaces: any = [];
	data_teams: any = [];

	data: any = [];
	data_filtered: any = [];
	refresh_event: any;

	video_fullfilepath: string;

	sort: string;

	backed: number = 0;

	uploading: boolean = false;
	prg: number = 0;

	// intent_params: any;
	intent_url: string = '';
	intent_uri: string = '';
	intent_type: string = '';

	extToMimes = [
		{ ext: 'jpeg', MType: 'image/jpeg' },
		{ ext: 'jpg', MType: 'image/jpeg' },
		{ ext: 'png', MType: 'image/png' },
		{ ext: 'doc', MType: 'application/msword' },
		{ ext: 'docx', MType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' },
		{ ext: 'xls', MType: 'application/vnd.ms-excel' },
		{ ext: 'xlsx', MType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' },
		{ ext: 'ppt', MType: 'application/vnd.ms-powerpoint' },
		{ ext: 'pptx', MType: 'application/vnd.openxmlformats-officedocument.presentationml.presentation' },
		{ ext: 'csv', MType: 'text/csv' },
		{ ext: 'gif', MType: 'image/gif' },
		{ ext: 'pdf', MType: 'application/pdf' },
		{ ext: 'mp4', MType: 'video/mp4' },
		{ ext: 'mp3', MType: 'audio/mpeg' },
	]

	constructor(
		public actionSheetController: ActionSheetController,
		public alertController: AlertController,
		public camera: Camera,
		public file: File,
		public fileChooser: FileChooser,
		public fileOpener: FileOpener,
		public filePath: FilePath,
		public global: GlobalProvider,
		public http: HTTP,
		public iab: InAppBrowser,
		public loadingController: LoadingController,
		public media: Media,
		public mediaCapture: MediaCapture,
		public modalController: ModalController,
		public ngZone: NgZone,
		public platform: Platform,
		public popoverController: PopoverController,
		public route: ActivatedRoute,
		public router: Router,
		public socialSharing: SocialSharing,
		public storage: Storage,
		public toastController: ToastController,
		public transfer: FileTransfer,
	) {

	}

	ngOnInit() {


		this.load_workspaces().then(() => {
			//----- SHARED INIT -----
			if (this.route.snapshot.paramMap.get('url')) { this.intent_url = this.route.snapshot.paramMap.get('url'); }

			if (this.route.snapshot.paramMap.get('uri')) { this.intent_uri = this.route.snapshot.paramMap.get('uri'); }

			if (this.route.snapshot.paramMap.get('type')) { this.intent_type = this.route.snapshot.paramMap.get('type'); }

			let intent_extension: any = this.route.snapshot.paramMap.get('extension');

			if (this.intent_type == 'image/jpeg' || this.intent_type == 'image/png' || this.intent_type == 'image/gif') { this.sgm_content = 'image'; }
			if (this.intent_type == 'video/mp4') { this.sgm_content = 'video'; }
			if (this.intent_type == 'audio/mpeg') { this.sgm_content = 'audio'; }
			if (
				this.intent_type == 'application/msword' ||
				this.intent_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
				this.intent_type == 'application/vnd.ms-powerpoint' ||
				this.intent_type == 'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
				this.intent_type == 'application/vnd.ms-excel' ||
				this.intent_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
				this.intent_type == 'text/csv' ||
				this.intent_type == 'application/pdf'
			) { this.sgm_content = 'file'; }

			// alert(this.intent_url);

			if (this.intent_url.length > 1) {
				this.sgm_content = 'web';

				this.storage.get('token').then((val_token) => {
					if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
					if (this.s_team == null) { this.s_team = '' }

					let url = this.global.baseURL + '/content-hub/store-web-link';
					let body = {
						url: this.intent_url
					};
					let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

					this.http.post(url, body, options).then(async res => {
						console.log(res.data);

						this.load_data();
					}).catch(async error => {
						console.log(error.error);
						error.error = JSON.parse(error.error);

						alert(JSON.stringify(error.error))
					});
				});
			} else if (this.intent_uri.length > 1) {
				this.filePath.resolveNativePath(this.intent_uri).then(filePath => {
					this.file.resolveLocalFilesystemUrl(this.intent_uri).then((fileEntry) => {
						fileEntry.getMetadata((metadata) => {

							let filesize = metadata.size.toFixed();
							let filename = filePath.substring(filePath.lastIndexOf('/') + 1);
							let fileext = filePath.substring(filePath.lastIndexOf('.') + 1);

							if (this.sgm_content == 'file' && fileext != 'pdf' && fileext != 'doc' && fileext != 'docx' && fileext != 'ppt' && fileext != 'pptx' && fileext != 'xls' && fileext != 'xlsx' && fileext != 'csv') {
								alert('File format not supported');
							} else {

								this.uploading = true;
								this.prg = 0;

								this.storage.get('token').then((val_token) => {
									let options: FileUploadOptions = {
										fileKey: 'file',
										fileName: filename,
										headers: { Accept: 'application/json', Authorization: 'Bearer ' + val_token }
									}

									if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
									if (this.s_team == null) { this.s_team = '' }

									const fileTransfer: FileTransferObject = this.transfer.create();
									const backend = this.global.baseURL + '/uploader' + '?type=' + this.sgm_content + '&content_type=' + fileext + '&size=' + filesize + '&file_name=' + filename + '&workspace_id=' + this.s_workspace + '&team_id=' + this.s_team;

									fileTransfer.upload(filePath, backend, options).then((entry) => {
										this.load_data();
										this.uploading = false;
									}, (error) => {
										alert(JSON.stringify(error));
										this.uploading = false;
									});

									fileTransfer.onProgress((e) => {
										this.ngZone.run(() => {
											this.prg = (e.lengthComputable) ? Math.floor((e.loaded / e.total * 100 + 1)) : -1;
										});
									});
								});
							}
						});
					});
				}).catch(err => alert('er : ' + JSON.stringify(err)));
			} else {
				this.load_data();
			}
		});
		//----- AUDIO -----
		this.global.obvAudio().subscribe((res) => {
			this.uploading = true;
			this.prg = 0;

			this.storage.get('token').then((val_token) => {
				let options: FileUploadOptions = {
					fileKey: 'file',
					fileName: res.filename,
					headers: { Accept: 'application/json', Authorization: 'Bearer ' + val_token }
				}

				// if (this.s_workspace == null) { this.s_workspace = '' }
				if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
				if (this.s_team == null) { this.s_team = '' }

				const fileTransfer: FileTransferObject = this.transfer.create();
				const backend = this.global.baseURL + '/uploader' + '?type=' + this.sgm_content + '&content_type=mp3&size=' + res.filesize + '&file_name=' + res.filename + '&workspace_id=' + this.s_workspace + '&team_id=' + this.s_team;

				fileTransfer.upload(res.filepath, backend, options).then((entry) => {
					this.load_data();
					this.uploading = false;
				}, (error) => {
					alert(JSON.stringify(error));
					this.uploading = false;
				});

				fileTransfer.onProgress((e) => {
					this.ngZone.run(() => {
						this.prg = (e.lengthComputable) ? Math.floor((e.loaded / e.total * 100 + 1)) : -1;
					});
				});

			});
		});
	}

	ionViewDidEnter() {
		// this.load_workspaces();
		// this.load_data();
		this.backToExit();
	}

	async load_workspaces() {
		this.data_workspaces = [];
		this.data_teams = [];

		this.workspace_id = null;

		this.storage.get('token').then((val_token) => {
			let url = this.global.baseURL + '/workspaces';
			let body = {};
			let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

			this.http.get(url, body, options).then(async res => {
				console.log(res.data);

				res.data = JSON.parse(res.data);

				for (let i = 0; i < res.data.length; i++) {
					this.data_workspaces.push(res.data[ i ]);

					if (res.data[ i ].name == 'Workspace Default') {
						this.s_workspace_default = res.data[ i ].id;
					}
				}
			}).catch(async error => {
				console.log(error.error);
				error.error = JSON.parse(error.error);
			});
		});
	}

	load_teams(val_id) {
		this.data_teams = [];

		if (val_id != undefined) {
			this.workspace_id = val_id;
		}

		this.storage.get('token').then((val_token) => {
			let url = this.global.baseURL + '/workspace/' + this.workspace_id + '/teams';
			let body = {};
			let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

			this.http.get(url, body, options).then(async res => {
				console.log(res.data);
				res.data = JSON.parse(res.data);

				this.data_teams = res.data;
			}).catch(async error => {
				console.log(error.error);
				error.error = JSON.parse(error.error);
			});
		});
	}

	select_ws() {
		this.s_team = '';

		if (this.s_workspace != '') {
			this.data_filtered = this.data.filter(f => f.workspace_id == this.s_workspace);
			this.load_teams(this.s_workspace)
		} else {
			this.load_data();
			// this.data_filtered = this.data;
			this.data_teams = [];
		}
	}

	select_tm() {
		if (this.s_team != '') {
			this.data_filtered = this.data.filter(f => f.workspace_id == this.s_workspace).filter(f => f.team_id == this.s_team);
		} else {
			this.data_filtered = this.data.filter(f => f.workspace_id == this.s_workspace);
		}
	}

	load_data() {
		this.platform.ready().then(() => {
			this.data_filtered = [];
			this.data = [];
			this.loading = true;

			this.storage.get('token').then((val_token) => {
				let url = this.global.baseURL + '/libraries?limit=';
				let body = {};
				let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

				this.http.get(url, body, options).then(res => {
					console.log(res.data);

					res.data = JSON.parse(res.data);

					for (let i = 0; i < res.data.data.length; i++) {
						this.data.push(res.data.data[ i ]);
					}

					this.ngZone.run(() => {
						if (this.s_workspace != '') {
							if (this.s_team != '') {
								this.data_filtered = this.data.filter(f => f.workspace_id == this.s_workspace).filter(f => f.team_id == this.s_team);
							} else {
								this.data_filtered = this.data.filter(f => f.workspace_id == this.s_workspace);
							}
						} else {
							this.data_filtered = this.data;
						}

						this.loading = false;
						this.refresh_event.target.complete();
					});

				}).catch(async error => {
					console.log(error.error);

					error.error = JSON.parse(error.error);

					alert('ERR : ' + error.error)
				});
			});
		});
	}

	doRefresh(event) {
		this.loading = true;

		this.refresh_event = event;
		this.load_data()
	}


	async share(val_content_id, val_url) {
		// alert(this.s_team);

		// if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
		// if (this.s_team == null) { this.s_team = '' }

		// const modal = await this.modalController.create({
		//     component: SharePage,
		//     componentProps: {
		//         'workspace_id': this.s_workspace,
		//         'team_id': this.s_team,
		//     }
		// });
		// modal.present();

		const actionSheet = await this.actionSheetController.create({
			header: 'Share',
			buttons: [ {
				text: 'Share to anyone',
				icon: 'people-circle-outline',
				handler: () => {
					let options = {
						url: val_url + '?content_id=' + val_content_id
					};
					this.socialSharing.shareWithOptions(options);
				}
			}, {
				text: 'share to CRM contact',
				icon: 'person-circle-outline',
				handler: async () => {
					const modal = await this.modalController.create({
						component: ContactsModalPage,
						componentProps: {
							url: val_url,
							content_id: val_content_id,
						}
					});
					modal.present();
				}
			}, {
				text: 'Cancel',
				icon: 'close',
				role: 'cancel',
				handler: () => {
					console.log('Cancel clicked');
				}
			} ]
		});
		await actionSheet.present();
	}


	upload_galeri() {
		let options = {
			// allowEdit: true,
			encodingType: this.camera.EncodingType.JPEG,
			// quality: 100,
			sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
			// targetHeight: 200,
			// targetWidth: 200,
		};

		this.camera.getPicture(options).then(async (imagePath) => {

			this.uploading = true;
			this.prg = 0;

			this.filePath.resolveNativePath(imagePath).then(filePath => {
				this.file.resolveLocalFilesystemUrl(imagePath).then((fileEntry) => {
					fileEntry.getMetadata((metadata) => {

						let filesize = metadata.size.toFixed();
						let filename = filePath.substring(filePath.lastIndexOf('/') + 1);
						let fileext = filePath.substring(filePath.lastIndexOf('.') + 1);

						this.storage.get('token').then((val_token) => {
							let options: FileUploadOptions = {
								fileKey: 'file',
								fileName: filename,
								headers: { Accept: 'application/json', Authorization: 'Bearer ' + val_token }
							}

							// if (this.s_workspace == null) { this.s_workspace = '' }
							if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
							if (this.s_team == null) { this.s_team = '' }

							const fileTransfer: FileTransferObject = this.transfer.create();
							const backend = this.global.baseURL + '/uploader' + '?type=' + this.sgm_content + '&content_type=' + fileext + '&size=' + filesize + '&file_name=' + filename + '&workspace_id=' + this.s_workspace + '&team_id=' + this.s_team;

							fileTransfer.upload(filePath, backend, options).then((entry) => {
								this.load_data();
								this.uploading = false;
							}, (error) => {
								alert(JSON.stringify(error));
								this.uploading = false;
							});

							fileTransfer.onProgress((e) => {
								this.ngZone.run(() => {
									this.prg = (e.lengthComputable) ? Math.floor((e.loaded / e.total * 100 + 1)) : -1;
								});
							});
						});

					});
				});
			}).catch(err => alert(err));
		}, (err) => {
			alert('Error while selecting image.');
			alert(JSON.stringify(err));
		});
	}

	upload_camera() {
		let options = {
			// allowEdit: true,
			encodingType: this.camera.EncodingType.JPEG,
			// quality: 100,
			sourceType: this.camera.PictureSourceType.CAMERA,
			// targetHeight: 200,
			// targetWidth: 200,
		};

		this.camera.getPicture(options).then(async (imagePath) => {

			this.uploading = true;
			this.prg = 0;

			this.filePath.resolveNativePath(imagePath).then(filePath => {
				this.file.resolveLocalFilesystemUrl(imagePath).then((fileEntry) => {
					fileEntry.getMetadata((metadata) => {

						let filesize = metadata.size.toFixed();
						let filename = filePath.substring(filePath.lastIndexOf('/') + 1);
						let fileext = filePath.substring(filePath.lastIndexOf('.') + 1);

						this.storage.get('token').then((val_token) => {
							let options: FileUploadOptions = {
								fileKey: 'file',
								fileName: filename,
								headers: { Accept: 'application/json', Authorization: 'Bearer ' + val_token }
							}

							// if (this.s_workspace == null) { this.s_workspace = '' }
							if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
							if (this.s_team == null) { this.s_team = '' }

							const fileTransfer: FileTransferObject = this.transfer.create();
							const backend = this.global.baseURL + '/uploader' + '?type=' + this.sgm_content + '&content_type=' + fileext + '&size=' + filesize + '&file_name=' + filename + '&workspace_id=' + this.s_workspace + '&team_id=' + this.s_team;

							fileTransfer.upload(filePath, backend, options).then((entry) => {
								this.load_data();
								this.uploading = false;
							}, (error) => {
								alert(JSON.stringify(error));
								this.uploading = false;
							});

							fileTransfer.onProgress((e) => {
								this.ngZone.run(() => {
									this.prg = (e.lengthComputable) ? Math.floor((e.loaded / e.total * 100 + 1)) : -1;
								});
							});
						});

					});
				});
			}).catch(err => alert(err));
		}, (err) => {
			//alert('Error while selecting image.');
			//this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
		});
	}

	upload_video() {
		this.mediaCapture.captureVideo().then(async (data: MediaFile[]) => {
			console.info(data);

			this.uploading = true;
			this.prg = 0;

			this.storage.get('token').then((val_token) => {
				let filename = data[ 0 ].fullPath.substring(data[ 0 ].fullPath.lastIndexOf('/') + 1);

				let options: FileUploadOptions = {
					fileKey: 'file',
					fileName: filename,
					headers: { Accept: 'application/json', Authorization: 'Bearer ' + val_token }
				}

				// if (this.s_workspace == null) { this.s_workspace = '' }
				if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
				if (this.s_team == null) { this.s_team = '' }

				const fileTransfer: FileTransferObject = this.transfer.create();
				const backend = this.global.baseURL + '/uploader' + '?type=video&content_type=mp4&size=' + data[ 0 ].size + '&file_name=' + filename + '&workspace_id=' + this.s_workspace + '&team_id=' + this.s_team;

				fileTransfer.upload(data[ 0 ].fullPath, backend, options).then((entry) => {
					this.load_data();
					this.uploading = false;
				}, (error) => {
					alert(JSON.stringify(error));
					this.uploading = false;
				});

				fileTransfer.onProgress((e) => {
					this.ngZone.run(() => {
						this.prg = (e.lengthComputable) ? Math.floor((e.loaded / e.total * 100 + 1)) : -1;
					});
				});

			});

		}, (err: CaptureError) => {
			alert(JSON.stringify(err))
		});
	}

	async upload_audio() {
		// if (this.s_workspace == null) { this.s_workspace = '' }
		if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
		if (this.s_team == null) { this.s_team = '' }

		const modal = await this.modalController.create({
			component: RecordAudioPage,
			componentProps: {
				'workspace_id': this.s_workspace,
				'team_id': this.s_team,
			}
		});
		modal.present();
	}

	upload_all() {
		let mimeType: any;

		if (this.sgm_content == 'video') { mimeType = { mime: 'video/mp4' }; }
		if (this.sgm_content == 'audio') { mimeType = { mime: 'audio/mpeg' }; }
		if (this.sgm_content == 'file') { mimeType = {}; }

		this.fileChooser.open(mimeType).then(async uri => {

			this.filePath.resolveNativePath(uri).then(filePath => {
				this.file.resolveLocalFilesystemUrl(uri).then((fileEntry) => {
					fileEntry.getMetadata((metadata) => {

						let filesize = metadata.size.toFixed();
						let filename = filePath.substring(filePath.lastIndexOf('/') + 1);
						let fileext = filePath.substring(filePath.lastIndexOf('.') + 1);

						if (this.sgm_content == 'file' && fileext != 'pdf' && fileext != 'doc' && fileext != 'docx' && fileext != 'ppt' && fileext != 'pptx' && fileext != 'xls' && fileext != 'xlsx' && fileext != 'csv') {
							alert('File format not supported');
						} else {

							this.uploading = true;
							this.prg = 0;

							this.storage.get('token').then((val_token) => {
								let options: FileUploadOptions = {
									fileKey: 'file',
									fileName: filename,
									headers: { Accept: 'application/json', Authorization: 'Bearer ' + val_token }
								}

								// if (this.s_workspace == null) { this.s_workspace = '' }
								if (this.s_workspace == null || this.s_workspace == '') { this.s_workspace = this.s_workspace_default }
								if (this.s_team == null) { this.s_team = '' }

								const fileTransfer: FileTransferObject = this.transfer.create();
								const backend = this.global.baseURL + '/uploader' + '?type=' + this.sgm_content + '&content_type=' + fileext + '&size=' + filesize + '&file_name=' + filename + '&workspace_id=' + this.s_workspace + '&team_id=' + this.s_team;

								fileTransfer.upload(filePath, backend, options).then((entry) => {
									this.load_data();
									this.uploading = false;
								}, (error) => {
									alert(JSON.stringify(error));
									this.uploading = false;
								});

								fileTransfer.onProgress((e) => {
									this.ngZone.run(() => {
										this.prg = (e.lengthComputable) ? Math.floor((e.loaded / e.total * 100 + 1)) : -1;
									});
								});

							});

						}
					});
				});
			}).catch(err => alert(err));
		}).catch(e => { console.log(e); });
	}

	async menu(val_id, val_name, val_url, val_type) {
		const actionSheet = await this.actionSheetController.create({
			header: val_name,
			mode: 'md',
			cssClass: 'my-custom-class',
			buttons: [
				{
					text: 'Open',
					icon: 'open-outline',
					handler: () => {
						if (val_type == 'web') {
							this.open_browser(val_url);
						} else {
							this.open_document(val_url, val_name);
						}
					}
				}, {
					text: 'Copy Content Team',
					icon: 'copy-outline',
					handler: () => {
						this.copy_team(val_id);
					}
				}, {
					text: 'Move Content Team',
					icon: 'arrow-forward-circle-outline',
					handler: () => {
						this.move_team(val_id);
					}
				}, {
					text: 'Share to anyone',
					icon: 'share-social-outline',
					handler: () => {
						let options = {
							url: val_url + '?content_id=' + val_id
						};
						this.socialSharing.shareWithOptions(options);
					}
				}, {
					text: 'Share to CRM contact',
					icon: 'share-social-outline',
					handler: async () => {
						const modal = await this.modalController.create({
							component: ContactsModalPage,
							componentProps: {
								url: val_url,
								content_id: val_id,
							}
						});
						modal.present();
					}
				}, {
					text: 'Delete',
					role: 'destructive',
					icon: 'trash-outline',
					handler: () => {
						this.delete(val_id);
					}
				}, {
					text: 'Cancel',
					icon: 'close',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				} ]
		});
		await actionSheet.present();
	}

	async open_document(val_url: string, val_title: string) {
		const loading = await this.loadingController.create({
			message: 'Downloading...',
		});
		await loading.present();

		let name = val_url.substring(val_url.lastIndexOf('/') + 1);

		const fileTransfer: FileTransferObject = this.transfer.create();
		const url = val_url;
		fileTransfer.download(url, this.file.dataDirectory + val_title).then((entry) => {
			this.fileOpener.open(entry.toURL(), this.getMimeByExt(name)).then(() => {
				console.log('File is opened');
				loading.dismiss();
			}).catch(e => {
				console.log('Error opening file', e);
				loading.dismiss();
			});
		}, (error) => {
			console.log(error);
			loading.dismiss();
		});
	}

	async open_browser(val_url) {
		this.iab.create(val_url, '_system', {});
	}

	getMimeByExt(name: any) {
		var extention = name.split('.').pop();
		for (let i = 0; i < this.extToMimes.length; i++) {
			const element = this.extToMimes[ i ];
			if (element.ext == extention) {
				return element.MType;
			}
		}
	}

	async copy_team(val_id) {
		let array_workspace: any = [];

		for (let i = 0; i < this.data_workspaces.length; i++) {
			array_workspace.push({
				name: this.data_workspaces[ i ].name,
				type: 'radio',
				label: this.data_workspaces[ i ].name,
				value: this.data_workspaces[ i ].id,
			});
		}

		const alert = await this.alertController.create({
			header: 'Select Workspace',
			mode: 'md',
			inputs: array_workspace,
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
					handler: () => {
						console.log('Confirm Cancel');
					}
				}, {
					text: 'Next',
					handler: (res: string) => {
						this.copy_team_sub(val_id, res);
					}
				}
			]
		});

		await alert.present();
	}

	async copy_team_sub(val_id, val_ws_id) {
		if (val_ws_id) {
			const loading = await this.loadingController.create({});
			await loading.present();

			let array_team: any = [];

			this.storage.get('token').then((val_token) => {
				let url = this.global.baseURL + '/workspace/' + val_ws_id + '/teams';
				let body = {};
				let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

				this.http.get(url, body, options).then(async res => {
					// console.info(res.data);
					res.data = JSON.parse(res.data);

					loading.dismiss();

					for (let i = 0; i < res.data.length; i++) {
						array_team.push({
							name: res.data[ i ].name,
							type: 'radio',
							label: res.data[ i ].name,
							value: res.data[ i ].id,
						});
					}

					const alert = await this.alertController.create({
						header: 'Select Team',
						mode: 'md',
						inputs: array_team,
						buttons: [
							{
								text: 'Cancel',
								role: 'cancel',
								cssClass: 'secondary',
								handler: () => {
									console.log('Confirm Cancel');
								}
							}, {
								text: 'Ok',
								handler: async (res: string) => {
									if (res) {
										console.info(val_id + ', ' + val_ws_id + ', ' + res);

										const loading = await this.loadingController.create({});
										await loading.present();

										//MOVE TEAM
										let url = this.global.baseURL + '/libraries/' + val_id + '/copy';
										let body = {
											id: val_id,
											workspace: val_ws_id,
											team: res,
										};
										let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

										this.http.post(url, body, options).then(async res => {
											console.info(res.data);
											res.data = JSON.parse(res.data);

											//TOAST SUCCESS
											const toast = await this.toastController.create({
												header: 'Content Hub Team Copied.',
												position: 'bottom',
												color: 'dark',
												duration: 2000
											});
											toast.present();

											this.load_data();
											loading.dismiss();

										}).catch(async error => {
											error.error = JSON.parse(error.error);
											console.error('ERR : ' + JSON.stringify(error.error));
											loading.dismiss();
										});
									} else {
										this.copy_team_sub(val_id, val_ws_id);
									}
								}
							}
						]
					});

					await alert.present();

				}).catch(async error => {
					console.error(error.error);
					error.error = JSON.parse(error.error);
					loading.dismiss();
				});
			});
		} else {
			alert('Please select workspace.');
			this.copy_team(val_id);
		}
	}

	async move_team(val_id) {
		let array_workspace: any = [];

		for (let i = 0; i < this.data_workspaces.length; i++) {
			array_workspace.push({
				name: this.data_workspaces[ i ].name,
				type: 'radio',
				label: this.data_workspaces[ i ].name,
				value: this.data_workspaces[ i ].id,
			});
		}

		const alert = await this.alertController.create({
			header: 'Select Workspace',
			mode: 'md',
			inputs: array_workspace,
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
					handler: () => {
						console.log('Confirm Cancel');
					}
				}, {
					text: 'Next',
					handler: (res: string) => {
						this.move_team_sub(val_id, res);
					}
				}
			]
		});

		await alert.present();
	}

	async move_team_sub(val_id, val_ws_id) {
		if (val_ws_id) {
			const loading = await this.loadingController.create({});
			await loading.present();

			let array_team: any = [];

			this.storage.get('token').then((val_token) => {
				let url = this.global.baseURL + '/workspace/' + val_ws_id + '/teams';
				let body = {};
				let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

				this.http.get(url, body, options).then(async res => {
					// console.info(res.data);
					res.data = JSON.parse(res.data);

					loading.dismiss();

					for (let i = 0; i < res.data.length; i++) {
						array_team.push({
							name: res.data[ i ].name,
							type: 'radio',
							label: res.data[ i ].name,
							value: res.data[ i ].id,
						});
					}

					const alert = await this.alertController.create({
						header: 'Select Team',
						mode: 'md',
						inputs: array_team,
						buttons: [
							{
								text: 'Cancel',
								role: 'cancel',
								cssClass: 'secondary',
								handler: () => {
									console.log('Confirm Cancel');
								}
							}, {
								text: 'Ok',
								handler: async (res: string) => {
									if (res) {
										console.info(val_id + ', ' + val_ws_id + ', ' + res);

										const loading = await this.loadingController.create({});
										await loading.present();

										//MOVE TEAM
										let url = this.global.baseURL + '/libraries/' + val_id + '/move';
										let body = {
											id: val_id,
											workspace: val_ws_id,
											team: res,
										};
										let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

										this.http.post(url, body, options).then(async res => {
											console.info(res.data);
											res.data = JSON.parse(res.data);

											//TOAST SUCCESS
											const toast = await this.toastController.create({
												header: 'Content Hub Team Moved.',
												position: 'bottom',
												color: 'dark',
												duration: 2000
											});
											toast.present();

											this.load_data();
											loading.dismiss();

										}).catch(async error => {
											error.error = JSON.parse(error.error);
											console.error('ERR : ' + JSON.stringify(error.error));
											loading.dismiss();
										});
									} else {
										this.move_team_sub(val_id, val_ws_id)
									}
								}
							}
						]
					});

					await alert.present();

				}).catch(async error => {
					console.error(error.error);
					error.error = JSON.parse(error.error);
					loading.dismiss();
				});
			});
		} else {
			alert('Please select workspace.');
			this.copy_team(val_id);
		}
	}

	async delete(val_id) {
		const alert = await this.alertController.create({
			header: 'Remove this item?',
			message: "You won't be able to revert this!",
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
					handler: (blah) => {
						console.log('Confirm Cancel: blah');
					}
				}, {
					text: 'Yes, delete it!',
					handler: () => {
						console.log('Confirm Okay');

						this.storage.get('token').then((val_token) => {
							let url = this.global.baseURL + '/libraries/' + val_id;
							let body = {};
							let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

							this.http.delete(url, body, options).then(res => {
								console.log(res.data);

								res.data = JSON.parse(res.data);

								this.load_data();

							}).catch(async error => {
								console.log(error.error);

								error.error = JSON.parse(error.error);

								console.error('ERR : ' + error.error)
							});
						});
					}
				}
			]
		});

		await alert.present();
	}

	backToExit() {
		this.backed = 0;

		this.platform.backButton.subscribe(async () => {
			if (this.router.url == '/home' && this.backed == 0) {
				const toast = await this.toastController.create({
					header: 'Press back again to exit.',
					position: 'bottom',
					color: 'dark',
					duration: 2000
				});
				toast.present();

				this.backed = 1;
				this.platform.backButton.subscribe(async () => {
					if (this.router.url == '/home' && this.backed == 1) {
						navigator[ 'app' ].exitApp();
					}
				});
			}
		});
	}

}

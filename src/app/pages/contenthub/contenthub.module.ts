import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContenthubPageRoutingModule } from './contenthub-routing.module';

import { ContenthubPage } from './contenthub.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContenthubPageRoutingModule
  ],
  declarations: [ContenthubPage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA ]
})
export class ContenthubPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContenthubPage } from './contenthub.page';

const routes: Routes = [
  {
    path: '',
    component: ContenthubPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContenthubPageRoutingModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CampaignsTeamPage } from './campaigns-team.page';

describe('CampaignsTeamPage', () => {
  let component: CampaignsTeamPage;
  let fixture: ComponentFixture<CampaignsTeamPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignsTeamPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CampaignsTeamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

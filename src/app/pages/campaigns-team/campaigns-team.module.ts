import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CampaignsTeamPageRoutingModule } from './campaigns-team-routing.module';

import { CampaignsTeamPage } from './campaigns-team.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CampaignsTeamPageRoutingModule
  ],
  declarations: [CampaignsTeamPage]
})
export class CampaignsTeamPageModule {}

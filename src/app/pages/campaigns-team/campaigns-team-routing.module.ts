import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampaignsTeamPage } from './campaigns-team.page';

const routes: Routes = [
  {
    path: '',
    component: CampaignsTeamPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampaignsTeamPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActionSheetController, ModalController, Platform, ToastController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { GlobalProvider } from '../../providers/global';
import { Router } from '@angular/router';

import { ContactsModalPage } from '../contacts-modal/contacts-modal.page';

@Component({
  selector: 'campaigns-team',
  templateUrl: './campaigns-team.page.html',
  styleUrls: [ './campaigns-team.page.scss' ],
})
export class CampaignsTeamPage implements OnInit {

  sgm_cmp = 'campaigns';

  loading: boolean = true;
  loading_contacts: boolean = true;

  data: any = [];

  data_contacts: any = [];
  data_contacts_f: any = [];

  team_id: string;

  mdl_search: string;

  refresh_event: any;

  backed: number = 0;

  permissions: any;

  contactShow:string;

  constructor(
    public actionSheetController: ActionSheetController,
    public global: GlobalProvider,
    public http: HTTP,
    public iab: InAppBrowser,
    public modalController: ModalController,
    public platform: Platform,
    public route: ActivatedRoute,
    public router: Router,
    public socialSharing: SocialSharing,
    public storage: Storage,
    public toastController: ToastController,
  ) {
    // this.events.subscribe('p_id_changed', (val) => {
    //   this.loading = true;
    //   this.load_campaigns();
    //   this.team_id = val;
    // });
  }

  async share(val_url) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Share',
      buttons: [ {
        text: 'Share to anyone',
        icon: 'people-circle-outline',
        handler: () => {
          let options = {
            url: val_url,
          };
          this.socialSharing.shareWithOptions(options);
        }
      }, {
        text: 'share to CRM contact',
        icon: 'person-circle-outline',
        handler: async () => {
          const modal = await this.modalController.create({
            component: ContactsModalPage,
            componentProps: {
              url: val_url,
              team_id: this.team_id,
              campaign_id: null,
            }
          });
          modal.present();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      } ]
    });
    await actionSheet.present();
  }

  ngOnInit() {
    // setInterval(() => {
    this.load_campaigns();
    this.load_contacts();
    // }, 3000);

    //CHECK PERSMISSIONS
    this.storage.get('permissions').then((res) => {
      this.permissions = res;

      if(this.permissions.contacts!='disabled'){
        this.contactShow='';
      }else{
        this.contactShow='none';
      }

    })
  }

  ionViewWillEnter() {
    this.team_id = this.route.snapshot.paramMap.get('team_id');
  }

  ionViewDidEnter() {
    this.backed = 0;

    this.platform.backButton.subscribe(async () => {
      if (this.router.url.startsWith('/campaigns-team') == true && this.backed == 0) {
        const toast = await this.toastController.create({
          header: 'Press back again to exit.',
          position: 'bottom',
          color: 'dark',
          duration: 2000
        });
        toast.present();

        this.backed = 1;
        this.platform.backButton.subscribe(async () => {
          if (this.router.url.startsWith('/campaigns-team') == true && this.backed == 1) {
            navigator[ 'app' ].exitApp();
          }
        });
      }
    });
  }

  doRefresh(event) {
    this.loading = true;

    this.refresh_event = event;
    this.load_campaigns();
    this.load_contacts();
  }

  load_campaigns() {
    this.data = [];

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/team/' + this.team_id + '/projects';
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(res_p => {
        console.log(res_p.data);

        res_p.data = JSON.parse(res_p.data);

        for (let n = 0; n < res_p.data.length; n++) {
          let url = this.global.baseURL + '/project/' + res_p.data[ n ].id + '/campaigns';
          let body = {};
          let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

          this.http.get(url, body, options).then(res => {
            console.log(res.data);

            res.data = JSON.parse(res.data);

            for (let i = 0; i < res.data.length; i++) {
              this.data.push(res.data[ i ]);
            }

            this.loading = false;
            this.refresh_event.target.complete();
          }).catch(async error => {
            console.log(error.error);

            error.error = JSON.parse(error.error);
          });
        }
      }).catch(async error => {
        console.log(error.error);

        error.error = JSON.parse(error.error);
      });
    });
  }

  openIAB(val_campaign_name, val_url) {
    // this.iab.create(val_url, '_blank', { hideurlbar: 'no' });
    this.router.navigate([ '/privatebrowser/' + '/' + val_campaign_name + '/' + val_url.replace("/", "%2F").replace("/", "%2F").replace("/", "%2F").replace("/", "%2F") ]);
  }

  openBrowser(val_url) {
    this.iab.create(val_url, '_system', { hideurlbar: 'yes' });
  }

  load_contacts() {
    this.data_contacts = [];

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/contacts/' + this.team_id;
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(res => {
        console.log(res.data);

        res.data = JSON.parse(res.data);

        for (let i = 0; i < res.data.data.length; i++) {
          if (!res.data.data[ i ].last_name) { res.data.data[ i ].last_name = ''; }

          this.data_contacts.push(res.data.data[ i ]);
        }

        // alert(JSON.stringify(this.data_contacts));

        this.data_contacts_f = this.data_contacts;

        this.loading = false;
        this.refresh_event.target.complete();
      }).catch(async error => {
        console.log(error.error);
        error.error = JSON.parse(error.error);
        console.error(JSON.stringify(error.error));
      });
    });
  }

  search_contacts() {
    this.data_contacts_f = this.data_contacts.filter((f) => {
      return f.first_name.toLowerCase().indexOf(this.mdl_search.toLowerCase()) > -1 || f.last_name.toLowerCase().indexOf(this.mdl_search.toLowerCase()) > -1 || f.email.toLowerCase().indexOf(this.mdl_search.toLowerCase()) > -1;
    });
  }

}
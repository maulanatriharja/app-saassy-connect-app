import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GlobalProvider } from '../../providers/global';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http/ngx';


@Component({
  selector: 'share',
  templateUrl: './share.page.html',
  styleUrls: [ './share.page.scss' ],
})
export class SharePage implements OnInit {

  @Input() workspace_id: string;
  @Input() team_id: string;

  data_project: any = [];
  data_campaign: any = [];

  constructor(
    public global: GlobalProvider,
    public http: HTTP,
    public modalController: ModalController,
    public socialSharing: SocialSharing,
    public storage: Storage,
  ) { }

  ngOnInit() {
    this.load_campaigns();
  }

  load_campaigns() {
    this.data_project = [];

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/team/' + this.team_id + '/projects';
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      alert(this.team_id);

      this.http.get(url, body, options).then(res_p => {
        console.log(res_p.data);

        res_p.data = JSON.parse(res_p.data);
        alert(3)
        for (let n = 0; n < res_p.data.length; n++) {
          let url = this.global.baseURL + '/project/' + res_p.data[ n ].id + '/campaigns';
          let body = {};
          let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

          this.http.get(url, body, options).then(res => {
            console.log(res.data);

            res.data = JSON.parse(res.data);

            for (let i = 0; i < res.data.length; i++) {
              this.data_project.push(res.data[ i ]);
            }
            alert(JSON.stringify(this.data_project));
          }).catch(async error => {
            console.log(error.error);

            error.error = JSON.parse(error.error);
            // alert("2" + JSON.stringify(error.error));
          });
        }
      }).catch(async error => {
        console.log(error.error);

        error.error = JSON.parse(error.error);
        // alert("1" + JSON.stringify(error.error));
      });
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global';

@Component({
  selector: 'password-reset',
  templateUrl: './password-reset.page.html',
  styleUrls: [ './password-reset.page.scss' ],
})
export class PasswordResetPage implements OnInit {

  logo: string;

  form_signin: FormGroup;

  loading_submit = false;
  input_type = 'password';
  show_pass = false;

  card_signin = true;

  constructor(
    public alertCtrl: AlertController,
    public global: GlobalProvider,
    public http: HTTP,
    public navCtrl: NavController,
    public router: Router,
    public statusBar: StatusBar,
    public storage: Storage,
    public toastController: ToastController
  ) {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.form_signin = new FormGroup({
      email: new FormControl('', [ Validators.required, Validators.pattern(EMAILPATTERN) ]),
    });
  }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.logo = 'assets/img/logo_' + this.global.whitelable + '.png';
  }

  async submit_send() {
    this.loading_submit = true;

    let url = this.global.baseURL + '/reset-password';
    let body = {
      email: this.form_signin.controls.email.value
    };

    let options = { Accept: 'application/json' };

    this.http.post(url, body, options).then(async data => {
      console.log(data.status);
      console.log(data.data);
      console.log(data.headers);

      data.data = JSON.parse(data.data);

      const alert = await this.alertCtrl.create({
        message: 'We have emailed your password reset link.',
        buttons: [
          {
            text: 'OK',
            handler: () => {
              this.navCtrl.navigateBack('/signin');
            }
          }
        ]
      });
      await alert.present();

      this.loading_submit = false;
    }).catch(async error => {

      console.log(error.status);
      console.log(error.error);
      console.log(error.headers);

      error.error = JSON.parse(error.error);

      if (error.error.error == 'Unauthorized') {
        const toast = await this.toastController.create({
          message: 'Email or password not valid.',
          position: 'bottom',
          color: 'danger',
          duration: 2000
        });
        toast.present();
      } else {
        if (!error.error.errors.email) { error.error.errors.email = ''; } else { error.error.errors.email = error.error.errors.email + '\n' }
        if (!error.error.errors.password) { error.error.errors.password = ''; }

        const toast = await this.toastController.create({
          message: error.error.errors.email + error.error.errors.password,
          position: 'bottom',
          color: 'danger',
          duration: 2000
        });
        toast.present();
      }

      this.loading_submit = false;
    });
  }

}

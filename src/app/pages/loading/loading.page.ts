import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, AlertController } from '@ionic/angular';
import { WebIntent } from '@ionic-native/web-intent/ngx';
import { Router } from '@angular/router';
import { GlobalProvider } from '../../providers/global';
import { HTTP } from '@ionic-native/http/ngx';


@Component({
  selector: 'loading',
  templateUrl: './loading.page.html',
  styleUrls: [ './loading.page.scss' ],
})
export class LoadingPage implements OnInit {

  constructor(
    public global: GlobalProvider,
    public http: HTTP,
    public navCtrl: NavController,
    public router: Router,
    public storage: Storage,
    public webIntent: WebIntent,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // this.storage.get('token').then((val_token) => {
    //   if (val_token) {
    //     this.navCtrl.navigateRoot('/home');
    //   } else {
    //     this.navCtrl.navigateRoot('/signin');
    //   }
    // }); 

    this.storage.get('token').then((val_token) => {
      if (val_token) {

        this.webIntent.getIntent().then((res) => {
          let url: string = '';
          if (res.extras) {
            if (res.extras[ "android.intent.extra.TEXT" ]) { url = res.extras[ "android.intent.extra.TEXT" ]; }
          }

          let url_permission = this.global.baseURL + '/limitation'; //CHECK PERMISSION
          let body = {};
          let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

          this.http.get(url_permission, body, options).then(async res_permission => {
            res_permission.data = JSON.parse(res_permission.data);

            if (res_permission.data) {
              if (res.clipItems != undefined && res_permission.data.permissions.growth.contenthub != 'disabled') {
                this.router.navigate([ '/contenthub', {
                  url: url,
                  uri: res.clipItems[ 0 ].uri,
                  type: res.clipItems[ 0 ].type,
                  extension: res.clipItems[ 0 ].extension
                } ]);
              }
            } else {
              this.navCtrl.navigateRoot('/home');
            }
          }).catch(async error => {
            console.log(error.error);
            error.error = JSON.parse(error.error);
          });

        });

      } else {
        this.navCtrl.navigateRoot('/signin');
      }
    });
  }

}

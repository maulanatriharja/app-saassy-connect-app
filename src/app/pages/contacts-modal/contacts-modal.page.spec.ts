import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContactsModalPage } from './contacts-modal.page';

describe('ContactsModalPage', () => {
  let component: ContactsModalPage;
  let fixture: ComponentFixture<ContactsModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContactsModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GlobalProvider } from '../../providers/global';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'contacts-modal',
  templateUrl: './contacts-modal.page.html',
  styleUrls: [ './contacts-modal.page.scss' ],
})
export class ContactsModalPage implements OnInit {

  data: any = [];
  loading: boolean;

  mdl_cari: string;

  @Input() url: string;
  @Input() team_id: string;
  @Input() campaign_id: string;
  @Input() content_id: string;

  constructor(
    public global: GlobalProvider,
    public http: HTTP,
    public modalController: ModalController,
    public socialSharing: SocialSharing,
    public storage: Storage,
  ) { }

  ngOnInit() {

  }

  load_data(ev: string) {
    this.data = [];
    this.loading = true;

    this.storage.get('token').then((val_token) => {
      let url: string;

      if (this.team_id) {
        url = this.global.baseURL + '/contacts/' + this.team_id;
      } else if (this.campaign_id) {
        url = this.global.baseURL + '/campaign/' + this.campaign_id + '/contacts';
      } else {
        url = this.global.baseURL + '/contacts';
      }

      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(res => {
        console.log(res.data);

        res.data = JSON.parse(res.data);

        for (let i = 0; i < res.data.data.length; i++) {
          if (!res.data.data[ i ].last_name) { res.data.data[ i ].last_name = ''; }
          if (!res.data.data[ i ].phone) { res.data.data[ i ].phone = ''; }
          if (!res.data.data[ i ].email) { res.data.data[ i ].email = ''; }

          if (
            res.data.data[ i ].first_name.toLowerCase().includes(this.mdl_cari.toLowerCase()) ||
            res.data.data[ i ].last_name.toLowerCase().includes(this.mdl_cari.toLowerCase()) ||
            res.data.data[ i ].email.toLowerCase().includes(this.mdl_cari.toLowerCase()) ||
            res.data.data[ i ].phone.toLowerCase().includes(this.mdl_cari.toLowerCase())
          ) {
            this.data.push(res.data.data[ i ]);
          }
        }

        // alert(JSON.stringify(this.data));

        this.loading = false;

      }).catch(async error => {
        console.log(error.error);

        error.error = JSON.parse(error.error);

        alert(JSON.stringify(error.error));
      });
    });
  }

  share(val_contact_id) {
    let options = {
      url: this.url + '?contact_id=' + val_contact_id + '&content_id=' + this.content_id,
    };
    this.socialSharing.shareWithOptions(options);
  }

  close() {
    this.modalController.dismiss();
  }

}

import { Component, Input, NgZone, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global';
import { HTTP } from '@ionic-native/http/ngx';


@Component({
  selector: 'record-audio',
  templateUrl: './record-audio.page.html',
  styleUrls: [ './record-audio.page.scss' ],
})
export class RecordAudioPage implements OnInit {

  @Input() workspace_id: string;
  @Input() team_id: string;

  status: string = 'start';
  filerecord: MediaObject;


  name: string;

  constructor(
    public file: File,
    public filePath: FilePath,
    public global: GlobalProvider,
    public http: HTTP,
    public loadingController: LoadingController,
    public ngZone: NgZone,
    public media: Media,
    public modalController: ModalController,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  recording() {
    this.status = 'recording';

    this.name = new Date().getFullYear() + '' + new Date().getMonth() + '' + new Date().getDate() + '' + new Date().getHours() + '' + new Date().getMinutes() + '' + new Date().getSeconds();

    this.filerecord = this.media.create(this.file.dataDirectory + this.name + '.mp3');
    this.filerecord.startRecord();
  }

  stoprecord() {
    this.status = 'stop';
    this.filerecord.stopRecord();
  }

  play() {
    this.status = 'play';
    this.filerecord.play();

    setInterval(() => {
      this.filerecord.getCurrentPosition().then((res) => {
        let position = res;
        if (position == -0.001) {
          this.stop()
        }
      });
    }, 500);
  }

  stop() {
    this.status = 'stop';
    this.filerecord.stop();

  }

  async upload() {

    this.filePath.resolveNativePath(this.file.dataDirectory + this.name + '.mp3').then(filePath => {
      this.file.resolveLocalFilesystemUrl(this.file.dataDirectory + this.name + '.mp3').then((fileEntry) => {
        fileEntry.getMetadata((metadata) => {

          let filesize = metadata.size.toFixed();
          let fileext = filePath.substring(filePath.lastIndexOf('.') + 1);
          let filename = filePath.substring(filePath.lastIndexOf('/') + 1);
          // if (this.s_workspace == null) { this.s_workspace = '' }
          // if (this.s_team == null) { this.s_team = '' }

          this.global.pubAudio({
            filepath: filePath,
            filename: filename,
            filesize: filesize
          });

          this.modalController.dismiss();

          // this.storage.get('token').then((val_token) => {
          //   let url = this.global.baseURL + '/uploader';
          //   let body = {
          //     type: 'audio',
          //     file_name: filename,
          //     content_type: 'mp3',
          //     size: filesize,
          //     workspace_id: this.workspace_id,
          //     team_id: this.team_id,
          //     // project_id: null,
          //     // campaign_id: null,
          //   }
          //   let options = { Accept: 'application/json', Authorization: 'Bearer ' + val_token };

          //   this.http.uploadFile(url, body, options, filePath.replace(" ", "%20"), 'file').then(res => {
          //     // alert('data : ' + res.data);
          //     this.global.pubAudio({});
          //     this.modalController.dismiss();
          //     loading.dismiss();
          //   }).catch(error => {
          //     console.info('er_data : ' + error.error);
          //     loading.dismiss();
          //   });
          // });
        });
      });
    })
  }

  close() {
    this.modalController.dismiss();
  }

}

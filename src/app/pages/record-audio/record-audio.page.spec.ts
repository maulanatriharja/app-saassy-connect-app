import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RecordAudioPage } from './record-audio.page';

describe('RecordAudioPage', () => {
  let component: RecordAudioPage;
  let fixture: ComponentFixture<RecordAudioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordAudioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RecordAudioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

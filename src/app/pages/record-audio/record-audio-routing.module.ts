import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecordAudioPage } from './record-audio.page';

const routes: Routes = [
  {
    path: '',
    component: RecordAudioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecordAudioPageRoutingModule {}

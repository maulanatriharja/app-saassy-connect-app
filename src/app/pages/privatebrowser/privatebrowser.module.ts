import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrivatebrowserPageRoutingModule } from './privatebrowser-routing.module';

import { PrivatebrowserPage } from './privatebrowser.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrivatebrowserPageRoutingModule
  ],
  declarations: [PrivatebrowserPage]
})
export class PrivatebrowserPageModule {}

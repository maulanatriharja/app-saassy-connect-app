import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrivatebrowserPage } from './privatebrowser.page';

const routes: Routes = [
  {
    path: '',
    component: PrivatebrowserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivatebrowserPageRoutingModule {}

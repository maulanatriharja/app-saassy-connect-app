import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'privatebrowser',
  templateUrl: './privatebrowser.page.html',
  styleUrls: [ './privatebrowser.page.scss' ],
})
export class PrivatebrowserPage implements OnInit {

  campaign_name: any;
  url: any;

  constructor(
    public route: ActivatedRoute,
    public sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    this.campaign_name = this.route.snapshot.paramMap.get('campaign_name');
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.route.snapshot.paramMap.get('url').replace("%2F", "/").replace("%2F", "/").replace("%2F", "/").replace("%2F", "/"));
  }

  doRefresh(event) {
    this.ngOnInit();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  test() {
    // alert(
    //JSON.stringify(document.getElementById('myiframe').addEventListener('onreadystatechange', {alert()}, false))
    // );
  }

}

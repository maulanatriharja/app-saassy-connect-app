import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrivatebrowserPage } from './privatebrowser.page';

describe('PrivatebrowserPage', () => {
  let component: PrivatebrowserPage;
  let fixture: ComponentFixture<PrivatebrowserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivatebrowserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrivatebrowserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

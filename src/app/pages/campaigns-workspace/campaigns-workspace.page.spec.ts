import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CampaignsWorkspacePage } from './campaigns-workspace.page';

describe('CampaignsWorkspacePage', () => {
  let component: CampaignsWorkspacePage;
  let fixture: ComponentFixture<CampaignsWorkspacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignsWorkspacePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CampaignsWorkspacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

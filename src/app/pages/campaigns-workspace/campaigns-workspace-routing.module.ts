import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampaignsWorkspacePage } from './campaigns-workspace.page';

const routes: Routes = [
  {
    path: '',
    component: CampaignsWorkspacePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampaignsWorkspacePageRoutingModule {}

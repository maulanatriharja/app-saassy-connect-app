import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CampaignsWorkspacePageRoutingModule } from './campaigns-workspace-routing.module';

import { CampaignsWorkspacePage } from './campaigns-workspace.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CampaignsWorkspacePageRoutingModule
  ],
  declarations: [CampaignsWorkspacePage]
})
export class CampaignsWorkspacePageModule {}

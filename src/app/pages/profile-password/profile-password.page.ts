import { AfterViewInit, Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController, AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global';


@Component({
  selector: 'profile-password',
  templateUrl: './profile-password.page.html',
  styleUrls: [ './profile-password.page.scss' ],
})
export class ProfilePasswordPage {

  user_id: string;
  user_name: string;
  user_first_name: string;
  user_last_name: string;
  user_email: string;
  avatar: string = 'assets/img/nophoto.jfif';

  myForm: FormGroup;

  input_type1 = 'password';
  input_type2 = 'password';
  input_type3 = 'password';
  show_pass1 = false;
  show_pass2 = false;
  show_pass3 = false;

  loading_submit = false;

  constructor(
    public actionSheetController: ActionSheetController,
    public alertCtrl: AlertController,
    public global: GlobalProvider,
    public http: HTTP,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public router: Router,
    public storage: Storage,
    public toastController: ToastController
  ) {

    this.myForm = new FormGroup({
      password_old: new FormControl('', [ Validators.required ]),
      password_new: new FormControl('', [ Validators.required ]),
      password_cnf: new FormControl('', [ Validators.required ]),
    });
  }

  ngOnInit() {
    this.load_profile();
  }

  async load_profile() {
    this.user_id = this.route.snapshot.paramMap.get('id');
    this.user_name = this.route.snapshot.paramMap.get('name');
    this.user_first_name = this.route.snapshot.paramMap.get('first_name');
    this.user_last_name = this.route.snapshot.paramMap.get('last_name');
    this.user_email = this.route.snapshot.paramMap.get('email');
    this.avatar = this.route.snapshot.paramMap.get('avatar').split('%2F').join('/');
  }

  eye_clicked1() {
    if (this.show_pass1 == false) {
      this.show_pass1 = true;
      this.input_type1 = 'text';
    } else {
      this.show_pass1 = false;
      this.input_type1 = 'password';
    }
  }

  eye_clicked2() {
    if (this.show_pass2 == false) {
      this.show_pass2 = true;
      this.input_type2 = 'text';
    } else {
      this.show_pass2 = false;
      this.input_type2 = 'password';
    }
  }

  eye_clicked3() {
    if (this.show_pass3 == false) {
      this.show_pass3 = true;
      this.input_type3 = 'text';
    } else {
      this.show_pass3 = false;
      this.input_type3 = 'password';
    }
  }

  async save() {
    this.loading_submit = true;

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/profile/update-password';
      let body = {
        current_password: this.myForm.controls.password_old.value,
        password: this.myForm.controls.password_new.value,
        password_confirmation: this.myForm.controls.password_cnf.value,
      };

      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.post(url, body, options).then(async res => {
        console.log(res.status);
        console.log(res.data);
        console.log(res.headers);

        res.data = JSON.parse(res.data);

        this.loading_submit = false;

        if (res.data.status == 'success') {
          const toast = await this.toastController.create({
            header: 'Password Updated.',
            position: 'top',
            color: 'success',
            duration: 2000
          });
          toast.present();

          this.navCtrl.navigateBack('/settings');
        } else {
          const toast = await this.toastController.create({
            message: res.data.message,
            position: 'top',
            color: 'danger',
            duration: 2000
          });
          toast.present();
        }

      }).catch(async error => {

        console.log(error.status);
        console.log(error.error);
        console.log(error.headers);

        error.error = JSON.parse(error.error);

        this.loading_submit = false;

        if (error.error.message == 'Unauthenticated.') {
          const toast = await this.toastController.create({
            message: 'Unauthenticated.',
            position: 'top',
            color: 'danger',
            duration: 2000
          });
          toast.present();
        } else {
          if (!error.error.errors.current_password) { error.error.errors.current_password = ''; } else { error.error.errors.current_password = error.error.errors.current_password + '\n' }
          if (!error.error.errors.password) { error.error.errors.password = ''; } else { error.error.errors.password = error.error.errors.password + '\n' }
          if (!error.error.errors.password_confirmation) { error.error.errors.password_confirmation = ''; } else { error.error.errors.password_confirmation = error.error.errors.password_confirmation + '\n' }

          if (!error.error.errors.message || error.error.errors.message == 'The given data was invalid.') {
            error.error.errors.message = '';
          }


          const toast = await this.toastController.create({
            message: error.error.errors.current_password + error.error.errors.password + error.error.errors.password_confirmation + error.error.errors.message,
            position: 'top',
            color: 'danger',
            duration: 2000
          });
          toast.present();
        }
      });
    });
  }

  cancel() {
    this.navCtrl.navigateBack('/settings')
  }
}

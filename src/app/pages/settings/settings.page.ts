import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { GlobalProvider } from '../../providers/global';

@Component({
  selector: 'settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  avatar: string = 'assets/img/nophoto.jfif';
  loading = true;
  data: any = [];

  backed: number = 0;

  constructor(
    public global: GlobalProvider,
    public http: HTTP,
    public navCtrl: NavController,
    public platform: Platform,
    public router: Router,
    public storage: Storage,
    public toastController: ToastController,
  ) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.load_profile();
  }

  ionViewDidEnter() {
    this.backed = 0;

    this.platform.backButton.subscribe(async () => {
      if (this.router.url == '/settings' && this.backed == 0) {
        const toast = await this.toastController.create({
          header: 'Press back again to exit.',
          position: 'bottom',
          color: 'dark',
          duration: 2000
        });
        toast.present();

        this.backed = 1;
        this.platform.backButton.subscribe(async () => {
          if (this.router.url == '/settings' && this.backed == 1) {
            navigator['app'].exitApp();
          }
        });
      }
    });
  }

  async load_profile() {
    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/profile';
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(async res => {
        console.log(res.status);
        console.log(res.data);
        console.log(res.headers);

        res.data = JSON.parse(res.data);
        this.data = res.data;

        if (res.data.avatar) { this.avatar = res.data.avatar; }

        this.loading = false;
      }).catch(async error => {

        console.log(error.status);
        console.log(error.error);
        console.log(error.headers);

        error.error = JSON.parse(error.error);

        this.loading = false;
      });
    });
  }

  profile_detail() {
    this.router.navigate(['/profile-details/' + this.data.id + '/' + this.data.name + '/' + this.data.first_name + '/' + this.data.last_name + '/' + this.data.email + '/' + this.avatar.split('/').join('%2F')])
  }

  profile_password() {
    this.router.navigate(['/profile-password/' + this.data.id + '/' + this.data.name + '/' + this.data.first_name + '/' + this.data.last_name + '/' + this.data.email + '/' + this.avatar.split('/').join('%2F')])
  }

  logout() {
    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/firebase/register-token';
      let body = { uid: '' };
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.post(url, body, options).then(async res => {
        console.info(res.data);
      }).catch(async error => {
        console.error(error.error);
      });
    });

    //this.storage.set('token', null);
    this.storage.clear();
    //this.router.navigateByUrl('/signin', { replaceUrl: true });
    this.navCtrl.navigateRoot('/signin');
  }
}

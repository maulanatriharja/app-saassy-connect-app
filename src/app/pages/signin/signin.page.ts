import { Component, OnInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global';


@Component({
  selector: 'signin',
  templateUrl: './signin.page.html',
  styleUrls: [ './signin.page.scss' ],
})
export class SigninPage implements OnInit {

  logo: string;

  form_signin: FormGroup;
  form_signup: FormGroup;

  loading_submit = false;
  input_type = 'password';
  show_pass = false;

  card_signin = true;
  card_signup = false;

  constructor(
    public global: GlobalProvider,
    public http: HTTP,
    public navCtrl: NavController,
    public router: Router,
    public statusBar: StatusBar,
    public storage: Storage,
    public toastController: ToastController
  ) {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.form_signin = new FormGroup({
      email: new FormControl('', [ Validators.required, Validators.pattern(EMAILPATTERN) ]),
      password: new FormControl('', [ Validators.required ]),
    });

    this.form_signup = new FormGroup({
      name: new FormControl('', [ Validators.required ]),
      email: new FormControl('', [ Validators.required, Validators.pattern(EMAILPATTERN) ]),
      password: new FormControl('', [ Validators.required ]),
    });
  }

  ngOnInit() {
    this.logo = 'assets/img/logo_' + this.global.whitelable + '.png';
  }

  ionViewWillEnter() {

    this.storage.get('token').then((val_token) => {
      if (val_token) {
        //this.router.navigateByUrl('/home', { replaceUrl: true });
        this.navCtrl.navigateRoot('/home');
      }
    });
  }

  eye_clicked() {
    if (this.show_pass == false) {
      this.show_pass = true;
      this.input_type = 'text';
    } else {
      this.show_pass = false;
      this.input_type = 'password';
    }
  }

  async submit_login() {
    this.loading_submit = true;

    let url = this.global.baseURL + '/login';
    let body = {
      email: this.form_signin.controls.email.value,
      password: this.form_signin.controls.password.value,
    };

    let options = { Accept: 'application/json' };

    this.http.post(url, body, options).then(async data => {
      console.log(data.status);
      console.log(data.data);
      console.log(data.headers);

      data.data = JSON.parse(data.data);

      if (data.data.success.token) {
        this.storage.set('token', data.data.success.token);
        // this.events.publish('login', 1);
        this.navCtrl.navigateRoot('/home');

        this.global.pubLogin({});
      } else {
        const toast = await this.toastController.create({
          header: 'Oops! Something went wrong, please try again.',
          position: 'bottom',
          color: 'danger',
          duration: 2000
        });
        toast.present();
      }

      this.loading_submit = false;
    }).catch(async error => {

      console.log(error.status);
      console.log(error.error);
      console.log(error.headers);

      error.error = JSON.parse(error.error);

      if (error.error.error == 'Unauthorized') {
        const toast = await this.toastController.create({
          message: 'Email or password not valid.',
          position: 'bottom',
          color: 'danger',
          duration: 2000
        });
        toast.present();
      } else {
        if (!error.error.errors.email) { error.error.errors.email = ''; } else { error.error.errors.email = error.error.errors.email + '\n' }
        if (!error.error.errors.password) { error.error.errors.password = ''; }

        const toast = await this.toastController.create({
          message: error.error.errors.email + error.error.errors.password,
          position: 'bottom',
          color: 'danger',
          duration: 2000
        });
        toast.present();
      }

      this.loading_submit = false;
    });
  }

}

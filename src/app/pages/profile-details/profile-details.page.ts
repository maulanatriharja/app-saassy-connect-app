import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController, AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { GlobalProvider } from '../../providers/global';


@Component({
  selector: 'profile-details',
  templateUrl: './profile-details.page.html',
  styleUrls: [ './profile-details.page.scss' ],
})
export class ProfileDetailsPage {

  user_id: string;
  user_name: string;
  user_first_name: string;
  user_last_name: string;
  user_email: string;
  avatar: string = 'assets/img/nophoto.jfif';

  myForm: FormGroup;

  constructor(
    public actionSheetController: ActionSheetController,
    public alertCtrl: AlertController,
    public camera: Camera,
    public global: GlobalProvider,
    public filePath: FilePath,
    public http: HTTP,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public router: Router,
    public storage: Storage,
    public toastController: ToastController
  ) {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    let f_name = '';
    let l_name = '';

    if (this.route.snapshot.paramMap.get('first_name') != 'null') {
      f_name = this.route.snapshot.paramMap.get('first_name');
    }

    if (this.route.snapshot.paramMap.get('last_name') != 'null') {
      l_name = this.route.snapshot.paramMap.get('last_name');
    }

    this.myForm = new FormGroup({
      first_name: new FormControl(f_name, [ Validators.required ]),
      last_name: new FormControl(l_name, [ Validators.required ]),
      email: new FormControl(this.route.snapshot.paramMap.get('email'), [ Validators.required, Validators.pattern(EMAILPATTERN) ]),
    });
  }

  ngOnInit() {
    this.load_profile();
  }

  async load_profile() {
    this.user_id = this.route.snapshot.paramMap.get('id');
    this.user_name = this.route.snapshot.paramMap.get('name');
    this.user_first_name = this.route.snapshot.paramMap.get('first_name');
    this.user_last_name = this.route.snapshot.paramMap.get('last_name');
    this.user_email = this.route.snapshot.paramMap.get('email');
    this.avatar = this.route.snapshot.paramMap.get('avatar').split('%2F').join('/');
  }

  async update_photo() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Update Photo',
      buttons: [ {
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          console.info('Open Camera');
          this.openCamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          console.info('Open Gallery');
          this.openGallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      } ]
    });
    await actionSheet.present();
  }

  openCamera() {
    let options = {
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      quality: 80,
      sourceType: this.camera.PictureSourceType.CAMERA,
      targetHeight: 200,
      targetWidth: 200,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.filePath.resolveNativePath(imagePath).then(async filePath => {

        const loading = await this.loadingController.create({
          message: 'Uploading image...'
        });
        await loading.present();

        this.storage.get('token').then((val_token) => {
          this.http.uploadFile(this.global.baseURL + '/profile/change-avatar', {}, { Accept: 'application/json', Authorization: 'Bearer ' + val_token },
            filePath, 'avatar').then(res => {
              console.info('status : ' + res.status);
              console.info('data : ' + res.data);
              console.info('headers : ' + JSON.stringify(res.headers));

              res.data = JSON.parse(res.data);

              if (res.data.avatar) { this.avatar = res.data.avatar; }

              this.global.pubProfile({});

              loading.dismiss();
            })
            .catch(error => {
              console.error('er_status : ' + error.status);
              console.error('er_data : ' + error.error);
              console.error('er_headers : ' + JSON.stringify(error.headers));

              error.data = JSON.parse(error.data);

              alert(error.data);

              loading.dismiss();
            });
        });
      });
    }, (err) => {
      //alert('Error while selecting image.');
      //this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
    });
  }

  openGallery() {
    let options = {
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetHeight: 200,
      targetWidth: 200,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.filePath.resolveNativePath(imagePath).then(async filePath => {
        const loading = await this.loadingController.create({
          message: 'Uploading image...'
        });
        await loading.present();

        this.storage.get('token').then((val_token) => {
          this.http.uploadFile(this.global.baseURL + '/profile/change-avatar', {}, { Accept: 'application/json', Authorization: 'Bearer ' + val_token },
            filePath, 'avatar').then(res => {
              console.info('status : ' + res.status);
              console.info('data : ' + res.data);
              console.info('headers : ' + JSON.stringify(res.headers));

              res.data = JSON.parse(res.data);

              if (res.data.avatar) { this.avatar = res.data.avatar; }

              this.global.pubProfile({});

              loading.dismiss();
            })
            .catch(error => {
              console.error('er_status : ' + error.status);
              console.error('er_data : ' + error.error);
              console.error('er_headers : ' + JSON.stringify(error.headers));

              error.data = JSON.parse(error.data);

              alert(error.data);

              loading.dismiss();
            });
        });
      });
    }, (err) => {
      //alert('Error while selecting image.');
      //this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
    });
  }

  async save() {
    const loading = await this.loadingController.create({
      message: 'Saving...'
    });
    await loading.present();

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/profile';
      let body = {
        first_name: this.myForm.controls.first_name.value,
        last_name: this.myForm.controls.last_name.value,
        email: this.myForm.controls.email.value,
      };

      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.put(url, body, options).then(async res => {
        console.log(res.status);
        console.log(res.data);
        console.log(res.headers);

        res.data = JSON.parse(res.data);

        await loading.dismiss();

        this.navCtrl.navigateBack('/settings');
        this.global.pubProfile({});

        const toast = await this.toastController.create({
          header: 'Profile Updated.',
          position: 'top',
          color: 'success',
          duration: 2000
        });
        toast.present();

      }).catch(async error => {

        console.log(error.status);
        console.log(error.error);
        console.log(error.headers);

        error.error = JSON.parse(error.error);

        await loading.dismiss();

        if (error.error.message == 'Unauthenticated.') {
          const toast = await this.toastController.create({
            message: 'Unauthenticated.',
            position: 'top',
            color: 'danger',
            duration: 2000
          });
          toast.present();
        } else {
          if (!error.error.errors.first_name) { error.error.errors.first_name = ''; } else { error.error.errors.first_name = error.error.errors.first_name + '\n' }
          if (!error.error.errors.last_name) { error.error.errors.last_name = ''; } else { error.error.errors.last_name = error.error.errors.last_name + '\n' }
          if (!error.error.errors.email) { error.error.errors.email = ''; } else { error.error.errors.email = error.error.errors.email + '\n' }

          const toast = await this.toastController.create({
            message: error.error.errors.first_name + error.error.errors.last_name + error.error.errors.email,
            position: 'top',
            color: 'danger',
            duration: 2000
          });
          toast.present();
        }
      });
    });
  }

  cancel() {
    this.navCtrl.navigateBack('/settings')
  }
}

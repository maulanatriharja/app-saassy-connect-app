import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { CheckTutorial } from './providers/check-tutorial.service';

const routes: Routes = [
  {
    path: '',
    //redirectTo: '/signin',
    redirectTo: '/loading',
    pathMatch: 'full'
  },
  {
    path: 'app',
    loadChildren: () => import('./pages/tabs-page/tabs-page.module').then(m => m.TabsModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'signin',
    loadChildren: () => import('./pages/signin/signin.module').then( m => m.SigninPageModule)
  },
  // {
  //   path: 'signup',
  //   loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)
  // },
  {
    path: 'campaigns/:project_id',
    loadChildren: () => import('./pages/campaigns/campaigns.module').then( m => m.CampaignsPageModule)
  },
  {
    path: 'teams',
    loadChildren: () => import('./pages/teams/teams.module').then( m => m.TeamsPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'password-reset',
    loadChildren: () => import('./pages/password-reset/password-reset.module').then( m => m.PasswordResetPageModule)
  },
  {
    path: 'campaigns-team/:team_id',
    loadChildren: () => import('./pages/campaigns-team/campaigns-team.module').then( m => m.CampaignsTeamPageModule)
  },
  {
    path: 'campaigns-workspace/:workspace_id',
    loadChildren: () => import('./pages/campaigns-workspace/campaigns-workspace.module').then( m => m.CampaignsWorkspacePageModule)
  },
  {
    path: 'loading',
    loadChildren: () => import('./pages/loading/loading.module').then( m => m.LoadingPageModule)
  },
  {
    path: 'profile-password/:id/:name/:first_name/:last_name/:email/:avatar', 
    loadChildren: () => import('./pages/profile-password/profile-password.module').then( m => m.ProfilePasswordPageModule)
  },
  {
    path: 'profile-details/:id/:name/:first_name/:last_name/:email/:avatar',
    loadChildren: () => import('./pages/profile-details/profile-details.module').then( m => m.ProfileDetailsPageModule)
  },
  {
    path: 'privatebrowser/:campaign_name/:url',
    loadChildren: () => import('./pages/privatebrowser/privatebrowser.module').then( m => m.PrivatebrowserPageModule)
  },
  {
    path: 'contenthub',
    loadChildren: () => import('./pages/contenthub/contenthub.module').then( m => m.ContenthubPageModule)
  },
  {
    path: 'submenu',
    loadChildren: () => import('./contenthub/submenu/submenu.module').then( m => m.SubmenuPageModule)
  },

  // {
  //   path: 'share',
  //   loadChildren: () => import('./pages/share/share.module').then( m => m.SharePageModule)
  // },

  // {
  //   path: 'contacts-modal',
  //   loadChildren: () => import('./pages/contacts-modal/contacts-modal.module').then( m => m.ContactsModalPageModule)
  // },

  // {
  //   path: 'record-audio',
  //   loadChildren: () => import('./pages/record-audio/record-audio.module').then( m => m.RecordAudioPageModule)
  // }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { Component, NgZone, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { MenuController, NavController, Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from './providers/global';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { WebIntent } from '@ionic-native/web-intent/ngx';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  // appPages = [
  //   {
  //     title: 'My Workspaces',
  //     url: '/app/tabs/schedule',
  //     icon: 'briefcase'
  //   },
  //   {
  //     title: 'Teams',
  //     url: '/app/tabs/speakers',
  //     icon: 'people'
  //   }
  // ];

  loggedIn = false;
  dark = false;

  minicon: string;
  app_name: string;

  data_profile: any = [];
  avatar = 'assets/img/nophoto.jfif';

  show_workspaces = false;

  permissions: any = {
    contacts: 'readwrite',
    contenthub: 'readwrite',
  };

  data_workspaces: any = [];
  data_teams: any = [];
  data_projects: any = [];
  data_campaigns: any = [];

  workspace_id: string;
  team_id: string;
  project_id: string;
  campaign_id: string;

  intent_data: any;

  constructor(
    public firebaseX: FirebaseX,
    public global: GlobalProvider,
    public http: HTTP,
    public menu: MenuController,
    public navCtrl: NavController,
    public ngZone: NgZone,
    public platform: Platform,
    public router: Router,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public storage: Storage,
    public swUpdate: SwUpdate,
    public toastCtrl: ToastController,
    public webIntent: WebIntent,
  ) {
    this.initializeApp();
  }

  async ngOnInit() {
    this.minicon = 'assets/img/minicon_' + this.global.whitelable + '.png';
    this.app_name = this.global.app_name;

    this.check_permission();
    this.load_profile();
    this.load_workspaces();
    this.post_fcm_token();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      //----------
      this.global.obvLogin().subscribe((data) => {
        this.load_profile();
        this.load_workspaces();
        this.post_fcm_token();
      });

      //----------
      this.global.obvProfile().subscribe((data) => {
        this.load_profile();
      });

      this.storage.get('token').then((val_token) => {
        if (val_token) {
          this.navCtrl.navigateRoot('/home');
        } else {
          this.navCtrl.navigateRoot('/signin');
        }
      });

      //-----LOGIN & WEB INTENT-----
      // this.storage.get('token').then((val_token) => {
      //   if (val_token) {
      //     this.webIntent.getIntent().then((res) => {

      //       let url: string = '';
      //       if (res.extras) {
      //         if (res.extras["android.intent.extra.TEXT"]) { url = res.extras["android.intent.extra.TEXT"]; }
      //       }

      //       if (res.clipItems != undefined) {
      //         this.router.navigate(['/contenthub', {
      //           url: url,
      //           uri: res.clipItems[0].uri,
      //           type: res.clipItems[0].type,
      //           extension: res.clipItems[0].extension
      //         }]);
      //       } else {
      //         this.navCtrl.navigateRoot('/home');
      //       }
      //     });
      //   } else {
      //     this.navCtrl.navigateRoot('/signin');
      //   }
      // });
    });
  }

  async post_fcm_token() {
    this.storage.get('token').then((val_token) => {
      this.firebaseX.getToken().then(fcm_token => {
        let url = this.global.baseURL + '/firebase/register-token';
        let body = { uid: fcm_token };
        let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

        this.http.post(url, body, options).then(async res => {
          console.log(res.data);
          //  alert(JSON.stringify(JSON.parse(res.data)));
        }).catch(async error => {
          console.log(error.error);
          this.post_fcm_token();
          //  alert(JSON.stringify(JSON.parse(error.error)));
        });
      });
    });
  }

  check_permission() {
    // this.permissions = [];

    this.storage.get('token').then((val_token) => {

      if (val_token) {

        let url = this.global.baseURL + '/limitation';
        let body = {};
        let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

        this.http.get(url, body, options).then(async res => {
          console.log(res.data);
          res.data = JSON.parse(res.data);

          // this.permissions.push({
          //   contacts: res.data.permissions.crm.contacts,
          //   contenthub: res.data.permissions.growth.contenthub,
          // });

          this.permissions = {
            contacts: res.data.permissions.crm.contacts,
            contenthub: res.data.permissions.growth.contenthub,
          };

          // this.storage.set('permissions', this.permissions[ 0 ]);

          this.storage.set('permissions', this.permissions);
        }).catch(async error => {
          console.log(error.error);
          error.error = JSON.parse(error.error);
        });
      } else {

        //JIKA API PERMISSIONS TIDAK DITEMUKAN
        // this.permissions.push({
        //   contacts: 'readwrite',
        //   contenthub: 'readwrite',
        // });
      }
    });
  }

  async load_profile() {
    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/profile';
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(async res => {
        console.log(res.data);

        res.data = JSON.parse(res.data);

        if (res.data.avatar) { this.avatar = res.data.avatar; } else {
          this.avatar = 'assets/img/nophoto.jfif';
        }

        this.data_profile = res.data;
      }).catch(async error => {
        console.log(error.error);

        error.error = JSON.parse(error.error);

        if (error.error.message == 'Unauthenticated.') {
          this.navCtrl.navigateRoot('/signin');
          this.storage.clear();
        }
      });
    });
  }

  //----------
  set_show_workspaces() {
    //this.menu.enable(true);

    if (this.show_workspaces == true) {
      this.show_workspaces = false;
    } else {
      this.show_workspaces = true;
    }

    //this.data_teams = [];
  }

  async load_workspaces() {
    this.data_workspaces = [];
    this.data_teams = [];
    this.data_projects = [];
    this.data_campaigns = [];

    this.workspace_id = null;

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/workspaces';
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(async res => {
        console.log(res.status);
        console.log(res.data);
        console.log(res.headers);

        res.data = JSON.parse(res.data);

        for (let i = 0; i < res.data.length; i++) {
          this.data_workspaces.push(res.data[ i ]);
        }
      }).catch(async error => {

        console.log(error.status);
        console.log(error.error);
        console.log(error.headers);

        error.error = JSON.parse(error.error);
      });
    });
  }

  load_teams(val_id) {
    // if (val_id != this.workspace_id) {

    this.data_teams = [];
    this.data_projects = [];
    this.data_campaigns = [];

    if (val_id != undefined) {
      this.workspace_id = val_id;
      this.team_id = null;
    }

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/workspace/' + this.workspace_id + '/teams';
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(async res => {
        console.log(res.status);
        console.log(res.data);
        console.log(res.headers);

        res.data = JSON.parse(res.data);

        this.data_teams = res.data;
      }).catch(async error => {

        console.log(error.status);
        console.log(error.error);
        console.log(error.headers);

        error.error = JSON.parse(error.error);
      });
    });

    // this.events.publish('p_id_changed', val_id);
    //return this.router.navigate([ '/campaigns-workspace/' + val_id ]);
    return this.navCtrl.navigateRoot([ '/campaigns-workspace/' + val_id ]);
    // }
  }

  load_projects(val_id) {
    // if (val_id != this.team_id) {
    this.data_projects = [];
    this.data_campaigns = [];

    if (val_id != undefined) {
      this.team_id = val_id;
      this.project_id = null;
    }

    this.storage.get('token').then((val_token) => {
      let url = this.global.baseURL + '/team/' + this.team_id + '/projects';
      let body = {};
      let options = { Authorization: 'Bearer ' + val_token, Accept: 'application/json' };

      this.http.get(url, body, options).then(async res => {
        console.log(res.status);
        console.log(res.data);
        console.log(res.headers);

        res.data = JSON.parse(res.data);

        this.data_projects = res.data;
      }).catch(async error => {

        console.log(error.status);
        console.log(error.error);
        console.log(error.headers);

        error.error = JSON.parse(error.error);
      });
    });

    // this.events.publish('p_id_changed', val_id);
    return this.navCtrl.navigateRoot([ '/campaigns-team/' + val_id ]);
    // }
  }

  goto_campaigns(val_project_id) {
    this.project_id = val_project_id;

    // this.events.publish('p_id_changed', val_project_id);
    return this.navCtrl.navigateRoot([ '/campaigns/' + val_project_id ]);
  }

}
